#|
  app-lib.lisp
|#

(in-package :cl)

(defpackage :app-lib
  (:use :cl)
  (:export :do-exit :get-args))

(in-package :app-lib)

(defun do-exit ()
 (format t "I'm quitting...~%")
 #+clozure (ccl:quit)
 #+cmu (extensions:quit)
 #+sbcl (sb-ext:exit)
 #+allegro (excl:exit))

(defun get-args-from (args flag)
  (let ((new-args (member flag args :test #'string=)))
   (if (null new-args)
     new-args
     (cdr new-args))))

(defun get-args ()
;;; from https://stackoverflow.com/questions/4285718/common-lisp-equivalent-to-haskells-main-function
;;; With help from Francois-Rene Rideau
;;; http://tinyurl.com/cli-args
 #+clisp ext:*args*
 #+sbcl (get-args-from sb-ext:*posix-argv* "--")
 #+clozure ccl:*unprocessed-command-line-arguments*
 #+gcl (get-args-from si:*command-args* "--")
 #+ecl (get-args-from (loop for i from 0 below (si:argc) collect (si:argv i)) "--")
 #+cmu (get-args-from extensions:*command-line-strings* "--")
 #+allegro (cdr (sys:command-line-arguments))
 #+lispworks sys:*line-arguments-list*)
