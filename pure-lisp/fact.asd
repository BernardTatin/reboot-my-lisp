
(asdf:defsystem #:fact
  ;;:depends-on ( #:drakma #:babel #:cl-csv #:yason #:url-rewrite)
  :components ((:file "./libs/app-lib")
                (:file "./applications/fact" :depends-on ("./libs/app-lib")))
  :name "factorial"
  :version "3.3"
  :maintainer "bTaT1"
  :author "bTaT1"
  :license "MIT"
  :description "Compute factorials"
  :long-description "Compute factorials")
