#|
  fact.lisp

  /usr/bin/sbcl --noinform --quit --load fact.lisp
  /usr/bin/gcl -batch -load fact.lisp
  ecl -q --load fact.lisp -- 15
  alisp --batch -L fact.lisp -- 15
  lisp -quiet -load fact.lisp -- 15

  ccl:
    ros use ccl-bin
    ros run --  -b -l fact.lisp -- 14
|#

(defun fact(N)
 (labels ((ifact (acc k)
           (if (< k 1)
            acc
            (ifact (* k acc) (- k 1)))))
         (ifact 1 N)))

(defun test(N)
  (when (> N 0)
    (format t "~&fac ~A = ~A~%" N (fact N))
    (test (- N 1))))


(defun dohelp ()
  (format t "<lisp> <lisp-options> <load-flag> fact.lisp -- N: compute N!~%~%")
  (app-lib:do-exit))


(defun main (args)
 (if (null args)
  (dohelp)
  (test (parse-integer (car args)))))

(main (app-lib::get-args))
#-(or sbcl cmu) (app-lib::do-exit)
